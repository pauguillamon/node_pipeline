const Calculator = require('./calculator')

const calc = new Calculator()

test('single addition', () => {
  expect(calc.eval("1 + 1")).toBe(2)
})

test('multiple addition', () => {
  expect(calc.eval("1 + 2 + 1")).toBe(4)
})

test('subtraction', () => {
  expect(calc.eval("3 - 5")).toBe(-2)
})

test('addition and subtraction', () => {
  expect(calc.eval("7 + 3 - 5")).toBe(5)
})

test('multiplication', () => {
  expect(calc.eval("3 * 5")).toBe(15)
})

test('division', () => {
  expect(calc.eval("3 / 5")).toBe(0.6)
})

test('order of operations', () => {
  expect(calc.eval("1 + 2 * 3 + 5")).toBe(12)
})

test('multiple order of operations', () => {
  expect(calc.eval("3 * 6 + 3 * 5")).toBe(33)
})
